﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Application;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public class EmployeesService : IEmployeesService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                throw new ArgumentException();
            }

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
