﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Application
{
    public interface IEmployeesService
    {
        Task UpdateAppliedPromocodesAsync(Guid employeeId);
    }
}
