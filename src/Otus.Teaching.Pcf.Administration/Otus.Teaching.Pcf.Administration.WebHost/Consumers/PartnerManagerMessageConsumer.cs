﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Application;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class PartnerManagerMessageConsumer : IConsumer<IPartnerManagerMessage>
    {
        private readonly IEmployeesService employeesService;

        public PartnerManagerMessageConsumer(IEmployeesService employeesService)
        {
            this.employeesService = employeesService;
        }

        public async Task Consume(ConsumeContext<IPartnerManagerMessage> context)
        {
            await employeesService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
        }
    }
}
