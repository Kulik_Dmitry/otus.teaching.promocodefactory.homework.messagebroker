﻿using MassTransit;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Administration.WebHost.Consumers;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class QueueWorker : BackgroundService
    {
        private readonly IBusControl _busControl;
        private readonly IServiceProvider _serviceProvider;

        public QueueWorker(IServiceProvider serviceProvider, IBusControl busControl)
        {
            _busControl = busControl;
            _serviceProvider = serviceProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var productChangeHandler = _busControl.ConnectReceiveEndpoint(x =>
            {
                x.Consumer<PartnerManagerMessageConsumer>(_serviceProvider);
            });

            await productChangeHandler.Ready;
        }
    }
}
