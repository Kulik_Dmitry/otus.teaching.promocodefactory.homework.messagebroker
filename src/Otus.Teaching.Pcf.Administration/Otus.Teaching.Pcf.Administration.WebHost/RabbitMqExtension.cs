﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administration.WebHost.Consumers;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public static class RabbitMqExtension
    {
        public static IServiceCollection RegisterQueueServices(this IServiceCollection services, IConfiguration config)
        {
            var rabbitMqSettings = config.GetSection("RabbitMq");
            var host = rabbitMqSettings.GetSection("HostName");
            var virtualHost = rabbitMqSettings.GetSection("VirtualHost");

            services.AddMassTransit(c =>
            {
                c.AddConsumer<PartnerManagerMessageConsumer>();
            });

            services.AddSingleton(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host(host.Value, virtualHost.Value);
            }));

            return services;
        }
    }
}
