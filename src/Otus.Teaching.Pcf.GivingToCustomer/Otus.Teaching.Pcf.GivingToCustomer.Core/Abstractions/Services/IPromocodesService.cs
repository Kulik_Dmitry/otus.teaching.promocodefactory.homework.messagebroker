﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromocodesService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerDto dto);
    }
}
