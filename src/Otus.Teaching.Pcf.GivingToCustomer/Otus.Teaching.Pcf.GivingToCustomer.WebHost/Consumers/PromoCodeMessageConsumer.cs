﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoCodeMessageConsumer : IConsumer<IPromoCodeMessage>
    {
        private readonly IPromocodesService promocodesService;

        public PromoCodeMessageConsumer(IPromocodesService promocodesService)
        {
            this.promocodesService = promocodesService;
        }

        public async Task Consume(ConsumeContext<IPromoCodeMessage> context)
        {
            await promocodesService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message.Dto);
        }
    }
}
