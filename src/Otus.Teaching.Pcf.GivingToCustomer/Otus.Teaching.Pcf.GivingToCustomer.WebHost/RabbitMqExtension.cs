﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public static class RabbitMqExtension
    {
        public static IServiceCollection RegisterQueueServices(this IServiceCollection services, IConfiguration config)
        {
            var rabbitMqSettings = config.GetSection("RabbitMq");
            var host = rabbitMqSettings.GetSection("HostName");
            var virtualHost = rabbitMqSettings.GetSection("VirtualHost");

            services.AddMassTransit(c =>
            {
                c.AddConsumer<PromoCodeMessageConsumer>();
            });

            services.AddSingleton(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host(host.Value, virtualHost.Value);
            }));

            return services;
        }
    }
}
