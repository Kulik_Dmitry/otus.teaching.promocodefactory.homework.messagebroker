﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public interface IPartnerManagerMessage
    {
        Guid Id { get; set; }

        Guid PartnerManagerId { get; set; }

        DateTime CreationDate { get; set; }
    }
}
