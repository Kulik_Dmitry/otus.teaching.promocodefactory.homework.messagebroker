﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PartnerManagerMessage : IPartnerManagerMessage
    {
        public Guid Id { get; set; }
        public Guid PartnerManagerId { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
