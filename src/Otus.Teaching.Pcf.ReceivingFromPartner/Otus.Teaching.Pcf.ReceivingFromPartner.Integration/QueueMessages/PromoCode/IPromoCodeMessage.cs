﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public interface IPromoCodeMessage
    {
        Guid Id { get; set; }

        GivePromoCodeToCustomerDto Dto { get; set; }

        DateTime CreationDate { get; set; } 
    }
}
