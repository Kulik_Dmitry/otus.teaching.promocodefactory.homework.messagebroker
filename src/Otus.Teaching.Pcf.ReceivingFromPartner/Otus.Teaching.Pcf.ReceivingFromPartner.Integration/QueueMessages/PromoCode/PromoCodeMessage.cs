﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class PromoCodeMessage : IPromoCodeMessage
    {
        public Guid Id { get; set; }

        public GivePromoCodeToCustomerDto Dto { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
